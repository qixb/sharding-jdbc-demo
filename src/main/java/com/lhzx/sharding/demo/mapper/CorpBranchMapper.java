package com.lhzx.sharding.demo.mapper;

import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Mapper
public interface CorpBranchMapper extends BaseMapper<CorpBranchEntity> {
}
