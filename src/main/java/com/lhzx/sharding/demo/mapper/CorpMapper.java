package com.lhzx.sharding.demo.mapper;

import com.lhzx.sharding.demo.entity.CorpEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Mapper
public interface CorpMapper extends BaseMapper<CorpEntity> {

    int batchUpdate(List<CorpEntity> list);

    int update(CorpEntity entity);

    List<CorpEntity> selectByCorpId(@Param("id") Long id, @Param("corpId") String corpId);
}
