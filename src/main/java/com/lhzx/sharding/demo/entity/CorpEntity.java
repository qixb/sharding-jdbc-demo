package com.lhzx.sharding.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_corp")
public class CorpEntity {
    private Long id;
    private String corpId;
    private String corpName;
}
