package com.lhzx.sharding.demo.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Component
public class ShardingUuidUtils {

    @Value("${sharding-ds-count}")
    private  int shardingDsCount;
    @Value("${sharding-table-count}")
    private  int shardingTableCount;

    private static AtomicInteger dsIndex = new AtomicInteger(0);
    private static AtomicInteger tableIndex = new AtomicInteger(0);

    public  String getShardingUuid() {
        String dsRouting = String.format("%02d", dsIndex.get() % shardingDsCount);
        String tableRouting = String.format("%02d", tableIndex.get() % shardingTableCount);
        if (shardingTableCount == tableIndex.incrementAndGet()) {
            tableIndex = new AtomicInteger(0);
            if (dsIndex.incrementAndGet() >= shardingDsCount) {
                dsIndex = new AtomicInteger(0);
            }
        }
        return UUID.randomUUID().toString().replace("-", "") + dsRouting + tableRouting;
    }

  /*  public static void main(String[] args) {
        for (int i = 0; i < 12; i++) {
            System.out.println(getUUid());
        }
    }*/

}
