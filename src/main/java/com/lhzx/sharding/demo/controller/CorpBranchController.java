package com.lhzx.sharding.demo.controller;

import com.google.common.collect.Lists;
import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import com.lhzx.sharding.demo.service.CorpBranchService;
import com.lhzx.sharding.demo.service.CorpService;
import com.lhzx.sharding.demo.utils.ShardingUuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@RestController
@RequestMapping("/corp/branch")
public class CorpBranchController {

    @Autowired
    private CorpBranchService corpBranchService;

    @Autowired
    private ShardingUuidUtils shardingUuidUtils;


    List<CorpBranchEntity> entities = Lists.newArrayList();

    @PostConstruct
    private void getData() {
        for(int i=0;i<=10;i++){
            entities.add(CorpBranchEntity.builder().corpId(shardingUuidUtils.getShardingUuid()).branchName("企业分支结构"+i).build());
        }
    }


    @RequestMapping("/add")
    public String addCorps() {
        corpBranchService.insertList(entities);
        return "success";
    }

    @RequestMapping("/list")
    public List<CorpBranchEntity> listCorp() {
        List<CorpBranchEntity> list = corpBranchService.selectAll();
        return list;
    }

}
