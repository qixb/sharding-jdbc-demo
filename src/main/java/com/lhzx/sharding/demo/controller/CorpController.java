package com.lhzx.sharding.demo.controller;

import com.google.common.collect.Lists;
import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import com.lhzx.sharding.demo.service.CorpService;
import com.lhzx.sharding.demo.utils.ShardingUuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@RestController
@RequestMapping("/corp")
public class CorpController {

    @Autowired
    private CorpService corpService;

    @Autowired
    private ShardingUuidUtils shardingUuidUtils;

    List<CorpEntity> corpEntities = Lists.newArrayList();

    @PostConstruct
    private void getData() {
        for(int i=0;i<=10;i++){
            corpEntities.add(CorpEntity.builder().corpId(shardingUuidUtils.getShardingUuid()).corpName("企业"+i).build());
        }
    }

    @RequestMapping("/add")
    public String addCorps() {
        corpService.insertList(corpEntities);
        return "success";
    }
    @RequestMapping("/update")
    public String updateCorps() {
        CorpEntity corpEntity=corpEntities.get(0);
        corpEntity.setCorpName("更新测试");
        corpService.update(corpEntity);
        return "success";
    }

    @RequestMapping("/list")
    public List<CorpEntity> listCorp() {
        List<CorpEntity> corpEntities = corpService.selectAll();
        return corpEntities;
    }
    @RequestMapping("/id")
    public List<CorpEntity> listCorp(@RequestParam(value = "corpId",required = false) String corpId,@RequestParam(value = "id",required = false) Long id) {
        List<CorpEntity> entitys = corpService.select(id, corpId);
        return entitys;
    }
    public static void main(String[] args) {

       /* String uuid = UUID.randomUUID().toString().replace("-", "") + "0102";
        System.out.println("uuid=" + uuid);*/
      /*  String substring = uuid.substring(uuid.length() - 4,uuid.length()-2);
        System.out.println("substring=" + substring);
        int i = Integer.parseInt(substring);
        System.out.println("index=" + i);*/
      for(int i=0;i<=100;i++){
          String uuid = UUID.randomUUID().toString().replace("-", "") + "0102";
          System.out.println("uuid=" + uuid);
      }
    }
}
