package com.lhzx.sharding.demo.service.impl;

import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import com.lhzx.sharding.demo.mapper.CorpBranchMapper;
import com.lhzx.sharding.demo.mapper.CorpMapper;
import com.lhzx.sharding.demo.service.CorpBranchService;
import com.lhzx.sharding.demo.service.CorpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Service
public class CorpBranchServiceImpl implements CorpBranchService {

    @Autowired
    private CorpBranchMapper corpBranchMapper;


    @Override
    public void insertList(List<CorpBranchEntity> list) {
        corpBranchMapper.insertList(list);
    }

    @Override
    public List<CorpBranchEntity> selectAll() {
        return corpBranchMapper.selectAll();
    }
}
