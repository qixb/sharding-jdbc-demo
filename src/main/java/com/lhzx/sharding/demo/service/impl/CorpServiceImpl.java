package com.lhzx.sharding.demo.service.impl;

import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import com.lhzx.sharding.demo.mapper.CorpBranchMapper;
import com.lhzx.sharding.demo.mapper.CorpMapper;
import com.lhzx.sharding.demo.service.CorpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
@Service
public class CorpServiceImpl implements CorpService {

    @Autowired
    private CorpMapper corpMapper;

    @Override
    public void insertList(List<CorpEntity> list) {
        int i = corpMapper.insertList(list);
    }

    @Override
    public void updateList(List<CorpEntity> list) {
        corpMapper.batchUpdate(list);
    }

    @Override
    public void update(CorpEntity entity) {
        int update = corpMapper.update(entity);
    }

    @Override
    public List<CorpEntity> selectAll() {
        return corpMapper.selectAll();
    }

    @Override
    public List<CorpEntity> select(Long id, String corpId) {
        return corpMapper.selectByCorpId(id, corpId);
    }

}
