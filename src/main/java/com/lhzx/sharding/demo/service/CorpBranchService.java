package com.lhzx.sharding.demo.service;

import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;

import java.util.List;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
public interface CorpBranchService {

    void insertList(List<CorpBranchEntity> list);

    List<CorpBranchEntity> selectAll();
}
