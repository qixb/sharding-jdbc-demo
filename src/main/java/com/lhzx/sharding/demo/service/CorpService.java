package com.lhzx.sharding.demo.service;

import com.lhzx.sharding.demo.entity.CorpBranchEntity;
import com.lhzx.sharding.demo.entity.CorpEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author qixingbo
 * @date 2020/11/3
 */
public interface CorpService {

    void insertList(List<CorpEntity> list);

    void updateList(List<CorpEntity> list);

    void update(CorpEntity entity);

    List<CorpEntity> selectAll();

    List<CorpEntity> select(Long id, String corpId);


}
