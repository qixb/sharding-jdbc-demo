drop table if exists t_corp0;
CREATE TABLE t_corp0 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  corp_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id),
  UNIQUE KEY uk_corp_id (corp_id)
)  COMMENT='企业信息表';

drop table if exists t_corp1;
CREATE TABLE t_corp1 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  corp_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id),
  UNIQUE KEY uk_corp_id (corp_id)
)  COMMENT='企业信息表';

drop table if exists t_corp2;
CREATE TABLE t_corp2 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  corp_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id),
  UNIQUE KEY uk_corp_id (corp_id)
)  COMMENT='企业信息表';


drop table if exists t_corp_branch0;
CREATE TABLE t_corp_branch0 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  branch_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业分支名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id)
)  COMMENT='企业分支机构表';

drop table if exists t_corp_branch1;
CREATE TABLE t_corp_branch1 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  branch_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业分支名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id)
)  COMMENT='企业分支机构表';
drop table if exists t_corp_branch2;
CREATE TABLE t_corp_branch2 (
  id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  corp_id varchar(64) NOT NULL DEFAULT '' COMMENT '企业id唯一标识',
  branch_name varchar(128) NOT NULL DEFAULT '' COMMENT '企业分支名称',
  create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
  update_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  update_by varchar(64) NOT NULL DEFAULT '' COMMENT '更新人,uuid',
  del_flag tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:记录有效,1:记录无效',
  PRIMARY KEY (id)
)  COMMENT='企业分支机构表';
